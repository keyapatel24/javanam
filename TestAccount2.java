

public class TestAccount2 {
    public static void main(String[] args) {
        Account[] arrAcc = new Account[5];
        Account.setInterestRate(1.05);

        for (int i =0; i < arrAcc.length; i++){
            arrAcc[i] = new SavingsAccount("Keya - " + i, (double) (i*5));
            System.out.println(arrAcc[i].getName() +" has the balance " + arrAcc[i].getBalance());
            arrAcc[i].addInterest();
            System.out.println("Now she has the balance " + arrAcc[i].getBalance());
            System.out.println(arrAcc[i].withdraw(7)+": " +arrAcc[i].getBalance());
            System.out.println();
        }
    }
}