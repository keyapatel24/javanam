public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new SavingsAccount(null, 0);
        myAccount.setName("Keya");
        myAccount.setBalance(1000);

        System.out.println("The account name is "+ myAccount.getName() + " the balance is " + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println("The account name is "+ myAccount.getName() + " the balance is " + myAccount.getBalance());
    }
}