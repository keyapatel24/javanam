public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = new Account[3];
        accounts[0] = new SavingsAccount("Account", 2);
        accounts[1] = new SavingsAccount("Savings Account", 4);
        accounts[2] = new CurrentAccount("Current Account", 6);

        for(Account a: accounts){
            a.addInterest();
            System.out.println(a.getName()+" has the balance: "+ a.getBalance());
        }
    }
}