public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] detailable = new Detailable[3];
        detailable[0] = new CurrentAccount();
        detailable[1] = new SavingsAccount();
        detailable[2] = new HomeInsurance();
        
        for (Detailable d: detailable){
            System.out.println(d.getDetails());
            
        }
    }
}