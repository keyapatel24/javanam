public abstract class Account implements Detailable{
    private double balance;
    private String name;
    static double interestRate;

    public double getBalance(){
        return balance;
    }
    public void setBalance(double balance){
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public Account(String name, double balance){
        this.name = name;
        this.balance = balance;
    }
    public Account(){
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }
    public boolean withdraw(double amount){
        if (balance > amount){
            balance-= amount;
            System.out.println("Successful transaction!");
            return true;
        }
        System.out.println("Unsuccessful transaction!");
        return false;
    }
    public boolean withdraw(){
        if (withdraw(20)){
            return true;
        }
        return false;
    }
    @Override
    public String getDetails() {
        return "" + name +" " + balance;
    }
}