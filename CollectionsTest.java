import java.util.HashSet;
import java.util.Iterator;

public class CollectionsTest {
    public static void main(String[] args) {
        HashSet<Account> accounts = new HashSet<Account>();
        accounts.add( new SavingsAccount("S1", 100));
        accounts.add( new CurrentAccount("C1", 200));
        accounts.add( new SavingsAccount("S2", 300));

        Iterator<Account> iter = accounts.iterator();
        
        while(iter.hasNext()){
            Account i = iter.next();
            System.out.println(i.getName() + " has: " + i.getBalance());
            i.addInterest();
            System.out.println("now it is: "+ i.getBalance());
        }
        
        for (Account a: accounts){
            System.out.println(a.getName() + " has: " + a.getBalance() );
            a.addInterest();
            System.out.println("now it is: "+ a.getBalance());
        }
    }
}